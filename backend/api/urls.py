from django.urls import path
from . import views

urlpatterns = [
    path('login', views.login_view),
    path('registro', views.registro_view),
    path('sesiones', views.sesiones_view),
    path('loaddata', views.loaddata),
    path('savedata', views.savedata),
    path('saveventa', views.saveventa),
    path('savelevelwin', views.savelevelwin),
    path('deletedata', views.deletedata),
    path('mercado', views.mercado),
    path('iterarModelo', views.iterarModelo),
]
