from django.contrib import admin
from django.contrib.admin.decorators import display
from django.contrib.admin.options import ModelAdmin
from django.forms.models import model_to_dict
from . import models

# Register your models here.
admin.site.register(models.Sesion, ModelAdmin)
admin.site.register(models.EstadoSesion, ModelAdmin)
admin.site.register(models.OfertasMercado, ModelAdmin)
admin.site.register(models.DatosSimulacion, ModelAdmin)
admin.site.register(models.Tabla, ModelAdmin)


@admin.register(models.TablaPunto)
class AdminTabla(ModelAdmin):
    list_display_links = ('tabla',)
    list_display = ('tabla','valorX', 'valorY')
    list_filter = ('tabla',)
    list_editable = ('valorX', 'valorY')
