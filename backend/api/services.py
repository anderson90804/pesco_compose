from .models import DatosJuego, DatosSimulacion, RetrasoData, Sesion, TablaPunto, Tabla, PrecioMercado
from django.forms.models import model_to_dict

def calcularMultiplicador(valorX, nombreTabla):

        tabla = Tabla.objects.get(nombreTabla=nombreTabla)

        menor = TablaPunto.objects.filter(tabla=tabla, valorX__lte = valorX).order_by('valorX').last()
        mayor = TablaPunto.objects.filter(tabla=tabla, valorX__gt = valorX).order_by('valorX').first()

        y = mayor.valorY if mayor is not None else None

        if menor is not None:   
           
            y1 = menor.valorY
            x1 = menor.valorX
            
            y = y1

            if mayor is not None:
                
                y2 = mayor.valorY
                x2 = mayor.valorX
                
                m = (y2-y1)/(x2 - x1)
                b = y1 - m*x1

                y = m*float(valorX) + b
            
        return y

class Retraso:
    def __init__(self, valorInicial, tiempoDeAjuste, nombreRetraso, nombreSimulacion):
        
        self.data, self.created = RetrasoData.objects.get_or_create(
            nombreRetraso = nombreRetraso,
            nombreSimulacion = nombreSimulacion,
            defaults= {
                'salidaRet1' : valorInicial,
                'salidaRet2' : valorInicial,
                'salidaRet3' : valorInicial
            }
        )

        self.tiempoDeAjuste = tiempoDeAjuste

    def iterar(self, entrada):
        flujo1 = (entrada - self.data.salidaRet1) / (self.tiempoDeAjuste/3)
        flujo2 = (self.data.salidaRet1 - self.data.salidaRet2) / (self.tiempoDeAjuste/3)
        flujo3 = (self.data.salidaRet2 - self.data.salidaRet3) / (self.tiempoDeAjuste/3)

        salida = self.data.salidaRet3

        self.data.salidaRet1=self.data.salidaRet1+flujo1
        self.data.salidaRet2=self.data.salidaRet2+flujo2
        self.data.salidaRet3=self.data.salidaRet3+flujo3

        self.data.save()

        return salida

class Simulacion:

    def __init__(self, sesion):
        self.sesion = sesion
        
        self.cobertura = 4
        self.produccionDiaria = 2000
        self.demora=15
        self.precioMinimo = 6500
        self.precioMaximo = 11000
        valorInicialRetraso = 2000
        
        self.retraso = Retraso(
            valorInicial=valorInicialRetraso,
            tiempoDeAjuste=130,
            nombreRetraso="RetrasoProduccionDemanda",
            nombreSimulacion="MercadoSimulacion"+str(sesion.id)
        )
        #self.salidaRet1 = valorInicialRetraso
        #self.salidaRet2 = valorInicialRetraso
        #self.salidaRet3 = valorInicialRetraso


    def iterar(self):
        datosSimulacion, laSimulacionFueCreada = DatosSimulacion.objects.get_or_create(sesion=self.sesion)
        
        productores = DatosJuego.objects.filter(estadoProduccion=True, sesion=self.sesion).count()

        produccionMaximaDiaria = productores*self.produccionDiaria
        
        #flujo1 = (produccionMaximaDiaria - self.salidaRet1) / (self.tiempoAjuste/3)
        #flujo2 = (self.salidaRet1 - self.salidaRet2) / (self.tiempoAjuste/3)
        #flujo3 = (self.salidaRet2 - self.salidaRet3) / (self.tiempoAjuste/3)
        
        demandaPotencial = self.retraso.iterar(produccionMaximaDiaria)
        #demandaPotencial = self.salidaRet3

        demanda = demandaPotencial*calcularMultiplicador(datosSimulacion.precioActual, 'Demanda')
        inventarioDeseado = demanda*self.cobertura

        radioInventario = 0
        
        if inventarioDeseado > 0:
            radioInventario = datosSimulacion.inventarioOferta/inventarioDeseado

        precioDeseado = calcularMultiplicador(radioInventario, 'Efecto del radio de inventario en el precio')

        precioObjetivo = \
            self.precioMaximo if precioDeseado > self.precioMaximo else\
            self.precioMinimo if precioDeseado < self.precioMinimo else\
            precioDeseado

        cambioPrecio = (precioObjetivo - datosSimulacion.precioActual) / self.demora

        ventasDemanda = datosSimulacion.inventarioOferta
        
        if ventasDemanda >= demanda:
            ventasDemanda = demanda
        
        inOferta = datosSimulacion.ventasProductores

        #self.salidaRet1=self.salidaRet1+flujo1
        #self.salidaRet2=self.salidaRet2+flujo2
        #self.salidaRet3=self.salidaRet3+flujo3

        datosSimulacion.inventarioOferta = datosSimulacion.inventarioOferta - ventasDemanda + inOferta
        datosSimulacion.precioActual = datosSimulacion.precioActual + cambioPrecio

        datosSimulacion.ventasProductores = 0
        datosSimulacion.tiempoSimulacion += 1

        if datosSimulacion.tiempoSimulacion % 7 == 0:
            datosSimulacion.turnoActual += 1
            if datosSimulacion.turnoActual > 6:
                datosSimulacion.turnoActual = 1

        datosSimulacion.save()
        
        PrecioMercado(
            precioMercado = datosSimulacion.precioActual,
            turnoActual = datosSimulacion.turnoActual,
            sesion = datosSimulacion.sesion
        ).save()        



