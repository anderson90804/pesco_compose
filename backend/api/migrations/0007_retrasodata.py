# Generated by Django 3.2 on 2021-06-07 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_alter_preciomercado_turnoactual'),
    ]

    operations = [
        migrations.CreateModel(
            name='RetrasoData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombreRetraso', models.TextField(max_length=50)),
                ('nombreSimulacion', models.TextField(max_length=50)),
                ('salidaRet1', models.FloatField()),
                ('salidaRet2', models.FloatField()),
                ('salidaRet3', models.FloatField()),
            ],
            options={
                'unique_together': {('nombreRetraso', 'nombreSimulacion')},
            },
        ),
    ]
