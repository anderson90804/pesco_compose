#!/bin/sh    
python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser \
                 --noinput \
                 --username $DJANGO_SUPERUSER_USERNAME \
                 --email $DJANGO_SUPERUSER_EMAIL &

echo "Starting Daphne!"
daphne -b 0.0.0.0 -p 8000 pesco.asgi:application